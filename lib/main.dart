import 'dart:io';

import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        brightness: Brightness.light,
        primaryColor: Colors.cyan,
      ),
      title: 'To Do List',
      home: MainScreen(),
    );
  }
}

class TodoScreen extends StatefulWidget {
  @override
  _TodoScreenState createState() => _TodoScreenState();
}

class _TodoScreenState extends State<TodoScreen> {
  TextEditingController t1 = TextEditingController();
  List<String> todoList = [];
  List<bool> done = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('To Do List'),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              color: Colors.blue.shade50,
              child: ListView.separated(
                itemBuilder: (BuildContext context, int index) {
                  return Dismissible(
                    key: Key(todoList[index]),
                    child: ListTile(
                      title: Text('${todoList[index]}'),
                      tileColor: Colors.black12,
                      onTap: () {
                        setState(() {
                          if (done[index]) {
                            done[index] = false;
                          } else {
                            done[index] = true;
                          }
                        });
                      },
                      trailing: IconButton(
                        icon: (done[index])
                            ? Icon(Icons.check_box_outlined)
                            : Icon(Icons.check_box_outline_blank_rounded),
                        onPressed: () {
                          setState(() {
                            if (done[index]) {
                              done[index] = false;
                            } else {
                              done[index] = true;
                            }
                          });
                        },
                      ),
                    ),
                    onDismissed: (direction) {
                      setState(() {
                        todoList.removeAt(index);
                        done.removeAt(index);
                      });
                    },
                  );
                },
                separatorBuilder: (BuildContext context, int index) {
                  return Divider(
                    color: Colors.black12,
                  );
                },
                itemCount: todoList.length,
              ),
            ),
          ),
          TextField(
            controller: t1,
            decoration: InputDecoration(
              hintText: 'Add a new task...',
              contentPadding: EdgeInsets.all(20.0),
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            todoList.add(t1.text);
            done.add(false);
            t1.clear();
          });
        },
        backgroundColor: Colors.blue.shade900,
        child: Icon(Icons.add),
      ),
    );
  }
}

class AboutScreen extends StatefulWidget {
  @override
  _AboutScreen createState() => _AboutScreen();
}

class _AboutScreen extends State<AboutScreen> {
  int counter = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Stack(
          overflow: Overflow.visible,
          alignment: Alignment.center,
          children: <Widget>[
            Image(
              image: NetworkImage(
                  'https://images.unsplash.com/photo-1542831371-29b0f74f9713?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=870&q=80'),
            ),
            Positioned(
              bottom: -50.0,
              child: CircleAvatar(
                radius: 80,
                backgroundColor: Colors.white,
                backgroundImage: NetworkImage(
                    'https://media-exp1.licdn.com/dms/image/C5603AQHxmBpl0SFDJw/profile-displayphoto-shrink_200_200/0/1626609122807?e=1650499200&v=beta&t=tx0CBwLAbfP3bVL631Dg_8BeP2GDJtlRLZxZSzf06tA'),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 50,
        ),
        ListTile(
          title: Center(child: Text('Rafli Bangsawan')),
          subtitle: Center(
            child: Text('You can call me Awan!'),
          ),
        ),
        ListTile(
          title: Padding(
            padding: const EdgeInsets.only(bottom: 10.0),
            child: Center(child: Text('About Me')),
          ),
          subtitle: Center(
            child: Text(
              'I am a passionate web and mobile developer who currently pursuing bachelor’s degree in computer science. I really love to explore new things especially when it comes to tech related.',
              textAlign: TextAlign.center,
            ),
          ),
        ),
        ListTile(
          title: Center(child: Text("Contact me on:")),
          subtitle: Center(child: Text("Instagram: @raflibangsawan")),
        ),
      ],
    );
  }
}

class MainScreen extends StatefulWidget {
  @override
  _MainScreen createState() => _MainScreen();
}

class _MainScreen extends State<MainScreen> {
  int navbarIdx = 0;
  final pages = [
    TodoScreen(),
    AboutScreen(),
  ];

  @override
  Widget build(BuildContext context) => Scaffold(
        body: pages[navbarIdx],
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: navbarIdx,
          onTap: (index) {
            setState(() {
              navbarIdx = index;
            });
          },
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
              backgroundColor: Colors.blue,
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'About',
              backgroundColor: Colors.white,
            ),
          ],
        ),
      );
}
